<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> LaraCms - @yield('title') </title>
    <style>
        .errors {
            border-radius: 5px;
            border: 1px solid #F00;
            background-color: #F66;
            margin-bottom: 5px;
        }
    </style>
</head>
<body>
    
    @yield('header')

    <hr>

    @yield('content')

    <hr>

    @yield('footer', 'A la prochaine !')

    <footer>
            Copyright ERN
    </footer>

</body>
</html>