@extends('layouts.app')

@section('title', 'articles')

@section('header')

    <h1> Tous les articles </h1>

    @auth
        <a href="{{ route('posts.create') }}"> Ajouter un article </a>
    @endauth

@endsection

@section('content')

    @include('posts.list')

@endsection