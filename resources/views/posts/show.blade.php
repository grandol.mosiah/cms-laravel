@extends('layouts.app')
@section('title', 'Article ' . $post->id)

@section('header')

    <h1> {{ $post->title }} </h1> 

@endsection

@section('content')

    <img src="{{ asset("storage/posts/{$post->image}") }}" width="300px" >

    <p> {{ $post->content }} </p>

    @auth
        @if( Auth::user()->id == $post->user_id )
            <ul>
            <li> <a href="{{ route('posts.edit', $post) }}"> Editer </a> </li>
                <li> 
                    <form action="{{ route('posts.destroy', $post) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Supprimer">
                    </form>
                </li>
            </ul>
        @endif
    @endauth

    <h3> Commentaires: </h3>

    @auth
        <form method="POST" action="{{ route('comments.store', $post) }}">
            @csrf
            <textarea name="content" ></textarea>
            <input type="submit" value="Commenter">
        </form>
    @endauth

    <ul>

        @foreach ($post->comments as $comment)
        
            <li> 
                <p> {{ $comment->content }} </p>
                @auth
                    <form method="POST" action="{{ route('comments.destroy', [$post, $comment]) }}" >
                        @csrf 
                        @method('DELETE')
                        <input type="submit" value="supprimer">
                    </form>
                @endauth
            </li>

        @endforeach

    </ul>

@endsection