@extends('layouts.app')

@section('title', 'Ajouter un article')

@section('header')
    <h1> Ajouter un article </h1>
@endsection

@section('content')

    @include('errors')

    <form action="{{ route('posts.update', $post) }}" method="POST">

        {{-- Champs de protection CSRF --}}
        @csrf

        {{-- Champs de type de requête --}}
        @method('PUT')

        <label>
            <span> Titre </span>
            <input type="text" name="title" id="title" value="{{ old('title') ?: $post->title }}">
        </label>

        <label>
            <span> Contenu </span>
            <textarea name="content" id="content">{{ old('content') ?: $post->content }}</textarea>
        </label>
        
        @foreach ($categories as $category)

            <div>
                <label>
                    <span>{{ $category->label }}</span>
                    <input 
                        name="categories[]" 
                        type="checkbox" 
                        value="{{ $category->id }}"
                        {{ $post->hasCategory( $category->id ) ? 'checked': '' }}
                    >
                </label>
            </div>

        @endforeach

        <input type="submit" value="Editer">

    </form>

@endsection