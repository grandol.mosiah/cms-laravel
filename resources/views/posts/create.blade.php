@extends('layouts.app')

@section('title', 'Ajouter un article')

@section('header')
    <h1> Ajouter un article </h1>
@endsection

@section('content')
    
    @include('errors')

    <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">

        {{-- Champs de protection CSRF --}}
        @csrf

        <label>
            <span> Titre </span>
            <input type="text" name="title" id="title" value="{{ old('title') }}">
        </label>

        <label>
            <span> Contenu </span>
            <textarea name="content" id="content">{{ old('content') }}</textarea>
        </label>

        <label>
            <span> Image </span>
            <input type="file" name="image">
        </label>

        @foreach ($categories as $category)

            <div>
                <label>
                    <span>{{ $category->label }}</span>
                    <input name="categories[]" type="checkbox" value="{{ $category->id }}">
                </label>
            </div>

        @endforeach

        <input type="submit" value="Ajouter">

    </form>

@endsection