<ul>

    @foreach ($posts as $post)
    
        <li>
            <h3> {{ $post->title }} </h3>
            <p> {{ $post->content }} </p>
            <p><i> {{ $post->user->name }} </i></p>
            <a href="{{ route('posts.show', $post) }}"> Voir </a>
        </li>

    @endforeach

</ul>