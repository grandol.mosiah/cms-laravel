<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {

    $files = scandir('./storage/app/public/posts');
    $files = array_slice($files, 2);

    //collect transforme n'importe quel tableau en collection laravel
    $file = collect($files)->random();

    return [
        'title' => $faker->sentence(4),
        'content' => $faker->text(100),
        'user_id' => App\User::all()->random()->id,
        'image' => $file?:''
    ];
});
