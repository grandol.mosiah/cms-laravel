<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Post;
use App\Category;
use App\Comment;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        factory( User::class, 10 )->create();
        factory( Post::class, 50)->create();
        factory( Category::class, 12)->create();
        factory( Comment::class, 100)->create();

        $categories = Category::all();

        foreach( Post::all() as $post ) {

            // On prend entre 1 et 3 categories au hasard
            $link_categories = $categories->random( rand(1, 3) );

            // On boucle sur les categories prises au hasard
            foreach( $link_categories as $link_category ) {

                // On effectue la liaison
                $post->categories()->attach( $link_category );

            }

        }

    }
}
