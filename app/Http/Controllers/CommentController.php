<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Auth;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    function __construct() {
        $this->middleware('auth');
        $this->middleware('check.author.comment', ['only'=>['destroy']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        
        $fields = $request->validate([
            'content' => 'required'
        ]);
        $fields['post_id'] = $post->id;
        $fields['user_id'] = Auth::user()->id;

        Comment::create( $fields );

        return redirect(route('posts.show', $post));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post, Comment $comment)
    {
        $comment->delete();

        return redirect( route('posts.show', $post) );
 
    }
}
