<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use Auth;

class PostController extends Controller
{

    function __construct() {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        $this->middleware('check.author', ['only' => ['edit', 'update', 'destroy']]);
    }
    
    function index() {

        $posts = Post::all();
        return view('posts.index', compact('posts'));

    }

    function show( Post $post ) {
        
        return view('posts.show', compact('post'));

    }

    function create() {
        $categories = Category::all();
        return view('posts.create', compact('categories'));
    }

    function store( Request $request ) {

        $fields = $request->validate([
            'title' => 'required|max:100',
            'content' => 'required',
            'categories' => 'required'
        ]);

        $fields['user_id'] = Auth::user()->id;

        $path = $request->file('image')->store('public/posts');
        $image = basename($path);

        $fields['image'] = $image;
        
        $post = Post::create( $fields );
        $post->addCategories( $fields['categories'] );

        return redirect( route('posts.show', $post) );
    }

    function edit( Post $post ) {

        $categories = Category::all();
        return view('posts.edit', compact('post', 'categories'));
    }

    function update( Post $post, Request $request ) {
        
        $fields = $request->validate([
            'title' => 'required|max:100',
            'content' => 'required',
            'categories' => 'required'
        ]);

        $post->update( $fields );
        $post->removeAllCategories();
        $post->addCategories( $fields['categories'] );

        return redirect( route('posts.show', $post) );

    }

    function destroy( Post $post ) {
            
        $post->delete();

        return redirect( route('posts.index') );
    }

}
