<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class MainController extends Controller
{
    
    function home() {
        $posts = Post::all();
        return view('home', compact('posts'));
    }

}
