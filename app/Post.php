<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    // Autorise l'hydratation des propriétés spécifiées
    protected $fillable = ['title', 'content', 'user_id', 'image'];

    // Empêche l'hydratation des données spécifiées
    // protected $guarded = ['_token'];

    function categories() {
        return $this->belongsToMany( Category::class );
    }

    function hasCategory( $id ) {
        return $this->categories->contains( $id );
    }

    function addCategories( array $categories ) {
        $this->categories()->attach( $categories );
    }

    function addCategory( int $id ) {
        if( $this->hasCategory($id) )
            return 0;
        else {
            $this->categories()->attach( $id );
            $this->categories->push( $id );
            return 1;
        }
    }

    function removeCategory( int $id ) {
        return $this->categories()->detach( $id );
    }

    function removeAllCategories() {
        return $this->categories()->detach();
    }

    function comments() {
        return $this->hasMany( Comment::class );
    }

    function user() {
        return $this->belongsTo( User::class );
    }

}
