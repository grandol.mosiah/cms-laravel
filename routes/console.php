<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

//
Artisan::command('hello {user}', function($user) {

    $this->comment("Hello {$user}!");

})->describe('Say hello to user');

//
Artisan::command('make:view {view}', function($view){
    
    $file = str_replace('.', '/', $view);
    file_put_contents("./resources/views/{$file}.blade.php", "@extends('layouts.app')");
    $this->comment("View {$view} was successfully created !");

})->describe('Create a view');