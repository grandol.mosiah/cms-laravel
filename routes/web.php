<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@home');

Route::get('/posts', 'PostController@index')
    ->name('posts.index');

Route::get('/posts/{post}/show', 'PostController@show')
    ->where('post', '[0-9]+')
    ->name('posts.show');

Route::get('/posts/create', 'PostController@create')
    ->name('posts.create');

Route::post('/posts/store', 'PostController@store')
    ->name('posts.store');

Route::get('/posts/{post}/edit', 'PostController@edit')
    ->where('post', '[0-9]+')
    ->name('posts.edit');

Route::put('/posts/{post}', 'PostController@update')
    ->where('post', '[0-9]+')
    ->name('posts.update');

Route::delete('/posts/{post}', 'PostController@destroy')
    ->where('post', '[0-9]+')
    ->name('posts.destroy');

Route::resource('categories', 'CategoryController')
    ->except(['show'])
    ->parameters([
        'category' => '[0-9]+'
    ]);

Route::resource('/posts/{post}/comments', 'CommentController')
    ->only(['store', 'destroy'])
    ->parameters([
        'post' => '[0-9]+',
        'comment' => '[0-9]+'
    ]);

//Authentication routes
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
